class Query {

    static getInsertQuery(label, properties) {
        return `CREATE (a:${label} ${Query.getPropertiesPlaceholder(properties)}) RETURN a`;
    }

    static getPropertiesPlaceholder(properties) {
        let result = [];
        Object.keys(properties).forEach(property => {
            result.push(`${property}:$${property}`);
        });
        return `{${result.join(', ')}}`
    }

    static getCreateRelationshipQuery(startNodeId, endNodeId, relationship) {
        const query = `
            MATCH (a),(b)
            WHERE a.node_id = '${startNodeId}' AND b.node_id = '${endNodeId}'
            CREATE (a)-[r:${relationship}]->(b)
            RETURN type(r)`;
        return query;
    }
}

module.exports = Query;
