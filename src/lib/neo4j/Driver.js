const driver = require('neo4j-driver').v1;
const Config = require('config');
class Driver {

    constructor() {
        this.driver = this.getDriver();
    }

    close() {
        this.driver.close();
    }

    getDriver() {
        const cfg = Config.get('neo4j');
        return driver.driver(
            `bolt://${cfg.neo4jHost}`, // neo4j uri
            driver.auth.basic(
                cfg.neo4jAuthUser, // neo4j user
                cfg.neo4jAuthPassword // neo4j password
            )
        );
    }
}
module.exports = new Driver();