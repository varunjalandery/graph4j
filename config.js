class Config {

    constructor() {
        this.configMapping = {};
        this.config = {};
        this.setConfigMapping();
        this.initConfig();

    }

    get(group = null) {
        if (group === null) {
            return this.config;
        }
        return this.config[group];
    }

    initConfig() {
        Object.keys(this.configMapping).forEach(configGroup =>  {
            this.setConfigGroup(configGroup);
        });
    }


    setConfigGroup(group) {
        this.config[group] = {};
        this.configMapping[group].forEach(envVar => {
            this.config[group][this.convertEnvVarToCamelCase(envVar)] = process.env[envVar];
        });
    }

    setConfigMapping() {
       this.configMapping.neo4j = [
           'NEO4J_HOST',
           'NEO4J_BOLT_PORT',
           'NEO4J_AUTH_USER',
           'NEO4J_AUTH_PASSWORD'
       ];
    }

    convertEnvVarToCamelCase(envVar) {
        let envVarCamCase = [];
        let lastCharUnderScore = false;
        let i = 0;
        while (i < envVar.length) {
            if (envVar[i] === '_') {
                lastCharUnderScore = true;
                i++;
                continue;
            }
            if (lastCharUnderScore) {
                lastCharUnderScore = false;
                envVarCamCase.push(envVar[i].toUpperCase());
            } else {
                envVarCamCase.push(envVar[i].toLowerCase());
            }
            i++;
        }
        return envVarCamCase.reduce((acc, cur) => acc + cur);
    }
}

module.exports = new Config();