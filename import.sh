#!/usr/bin/env bash

bin/neo4j-import \
--into /var/lib/neo4j/data/databases/graph.db \
--nodes:Entity /dumps/paradise_papers.nodes.entity.csv \
--nodes:Address /dumps/paradise_papers.nodes.address.csv \
--nodes:Officer /dumps/paradise_papers.nodes.officer.csv  \
--nodes:Intermediary /dumps/paradise_papers.nodes.intermediary.csv  \
--nodes:Other /dumps/paradise_papers.nodes.other.csv  \
--relationships /dumps/paradise_papers.edges.csv